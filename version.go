package otel

// Version is the current release version of OpenTelemetry Logs in use.
func Version() string {
	return "0.0.1"
}
